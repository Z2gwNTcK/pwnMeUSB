#/bin/bash

message="Unfortunately, $LOGNAME has left their laptop open and unlocked. Some cheeky hacker might have done some not
		so nice things given this advantageous opportunity. With information security being so important these days, I'm sure
		this was just a minor oversight which will get corrected immediately. In the meantime, please enjoy this
		delightful musical selection, courtesy of $LOGNAME. Cheers!"
desktopImage="wallpaper.png"
mp3File="audio.mp3"
usbPath="/Volumes/PWNME"
sourcePath="$usbPath/.source"

function copyFile(){
	cp $sourcePath/$1 ~/$2/$1
}

function changeDesktopImageAndDelete(){
	osascript -e "tell application \"Finder\" to set desktop picture to POSIX file \"/Users/$LOGNAME/Pictures/$desktopImage\""
	sleep 1
	rm ~/Pictures/$desktopImage
}

function showDesktopBackground(){
	osascript -e "tell application \"Finder\"
		activate
		set visible of every process whose name is not \"Finder\" to false
		end tell"
}

function setVolume(){
	osascript -e "set Volume $1"
}

function danielSays(){
	say -v Daniel $1
}

function unmountAndLightTheFuse(){
	diskutil unmount $usbPath && sleep 15 && danielSays "$message" && afplay ~/$2/$1 && rm ~/$2/$1 &
}

copyFile $desktopImage "Pictures"
copyFile $mp3File "Music"
changeDesktopImageAndDelete
showDesktopBackground
setVolume "10"
unmountAndLightTheFuse $mp3File "Music"
